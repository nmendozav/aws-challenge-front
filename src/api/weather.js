import axiosInstance from '../util/conn'

export default class Weather {
    static getData = (text) => {
        return new Promise((resolve, reject)=>{
            axiosInstance.get(`/weather?text=${text}`)
            .then(response => {
                resolve(response.data);
            }).catch(e=>{
                console.log(e);
                reject(e);
            });
        });
    }
}
