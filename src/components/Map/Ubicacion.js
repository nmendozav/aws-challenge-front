import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import Grid from '@material-ui/core/Grid';
import {Map, Marker, GoogleApiWrapper} from 'google-maps-react';

const useStyles = makeStyles((theme) => ({
	root: {
		maxWidth: '100%',
		maxHeight: '100%',
		height: 550,
		margin: '1%',
	  },
  image: {
    width: '100%',
    height:  '100%',
  },
  image2: {
    width: '100%',
	height:  '100%',
	padding: 1,
  },
  toolbarTitle: {
    flex: 1,
  },
  toolbar: {
    borderBottom: `1px solid ${theme.palette.divider}`,
    marginBottom: '12px',
    marginTop: '3%',
  },
}));

export function Ubicacion(props) {
	const classes = useStyles();
	//console.log({ lat: props.lat, lng: props.lon});
	const onMapClicked = (props)=> {}
  	return (
		<Grid container>
			<Grid item xs={12} justify="center" container>
				<Typography
					component="h2"
					variant="h5"
					color="inherit"
					align="center"
					noWrap
					className={classes.toolbarTitle}
				>
					UBICACIÓN
				</Typography>
			</Grid>
			<Grid item xs={12}>
				<Card className={classes.root}>
					<CardActionArea className={classes.image}> 
						<CardContent className={classes.image2}>
							<Map 
								google={props.google} 
								zoom={6}
								center={{ lat: props.lat, lng: props.lon}} 
								onClick={onMapClicked} className={classes.image}>
								<Marker position={{ lat: props.lat, lng: props.lon }}>
								</Marker>
							</Map>
						</CardContent>
					</CardActionArea>
				</Card>
			</Grid>
		</Grid>
  	);
}

export default GoogleApiWrapper({
	apiKey: 'AIzaSyA86hiBxRbClDtZsq7LtwysI5VS7U1PGXo'
})(Ubicacion)