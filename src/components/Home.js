import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Ubicacion from './Map/Ubicacion'
import Weather from '../api/weather';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Detalle from './Details/Detalle';
import SearchIcon from '@material-ui/icons/Search';

import Button from '@material-ui/core/Button';
//import ChartData from './Chart/Chart';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
}));

function Home() {
  const classes = useStyles();

  const [lat, setLat] = useState(0);
  const [lon, setLon] = useState(0);
  const [dataClima, setDataClima] = useState(0);
  const [text, setText] = useState('Santiago');

  const onValueReceive = event =>{
    setText(event.target.value)
  }
  
  useEffect(() => {
    Weather.getData(text).then(res=>{
      //console.log("RES", res);
      if(res.body.city){
        setLat(res.body.city?.coord?.lat);
        setLon(res.body.city?.coord?.lon);
      }
      setDataClima(res.body);
    }).catch(e=>{
      console.log("ERR:", e);
    })
  },[]);

  const buscarCiudad = () =>{
    Weather.getData(text).then(res=>{
      //console.log("RES", res);
      if(res.body.city){
        setLat(res.body.city?.coord?.lat);
        setLon(res.body.city?.coord?.lon);
      }
      setDataClima(res.body);
    }).catch(e=>{
      console.log("ERR:", e);
    })
  }

  return (
    <div className={classes.root}>
      <Grid container justify="center" spacing={1}>
        <Paper className={classes.paper} >
          <Grid xs={12} justify="center" container item spacing={1}>
              <Grid xs={6} item >
                  <TextField
                    label="Ingresa Ciudad"
                    id="outlined-size-small"
                    variant="outlined"
                    size="small"
                    value={text}
                    onChange={onValueReceive}
                    fullWidth
                  />
              </Grid>
              <Grid xs={6} item>
                  <Button variant="contained" color="primary" onClick={buscarCiudad} startIcon={<SearchIcon />}>
                    Buscar
                  </Button>
              </Grid>
          </Grid>
        </Paper>

        {/*<Grid item xs={12} justify="center" container spacing={1}>
          <Grid xs={6} item>
            <Paper className={classes.paper} >
              <ChartData data={dataClima}/>
            </Paper>
          </Grid>
        </Grid>*/}

        <Grid item xs={12} justify="center" container spacing={1}>
          <Grid xs={3} item>
            <Paper className={classes.paper} >
              <Detalle data={dataClima}/>
            </Paper>
          </Grid>
          <Grid xs={3} item>
            <Paper className={classes.paper} >
              <Ubicacion lat={lat} lon={lon} text={text}/>
            </Paper>
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
}

export default Home;
