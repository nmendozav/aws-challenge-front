import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import ItemDetalle from './ItemDetalle';
import MuiAlert from '@material-ui/lab/Alert';

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const useStyles = makeStyles((theme) => ({
	root: {
		maxWidth: '100%',
		maxHeight: '100%',
		minHeight: 500,
		height: 'auto',
		margin: '1%',
	  },
	card: {
		width: '100%',
		height:  '100%',
	},
	toolbarTitle: {
		flex: 1,
	},
}));

export default function Detalle(props) {
	const classes = useStyles();
	const [expanded, setExpanded] = React.useState(0);

	const setViewDetalle = (dt)=>{
		if(expanded === dt){
			setExpanded('');
		}else{
			setExpanded(dt);
		}
	}

	return (
		<Grid container spacing={1}>
			{
				props.data.cod==='200'?
					<Grid item xs={12} className={classes.root} container spacing={1}>
					{	props.data.list?.map(data => <ItemDetalle key={data.dt} info={data} setOpenDetails={setViewDetalle} expanded={expanded}/>) }
					</Grid>
				:
				<Grid item xs={12} className={classes.root}>
					<Alert severity="info">{props.data.message}</Alert>
				</Grid>
			}
		</Grid>
	);
}