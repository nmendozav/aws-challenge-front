import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardHeader from '@material-ui/core/CardHeader';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import CardContent from '@material-ui/core/CardContent';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { red, green, lightBlue } from '@material-ui/core/colors';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
const moment = require('moment-timezone');

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 345,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatarH: {
    backgroundColor: red[500],
	},
	avatarM: {
    backgroundColor: green[500],
	},
	avatarL: {
    backgroundColor: lightBlue[500],
  },
}));

function diaSemana(dia,mes,anio){
	var dias=["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"];
	var dt = new Date(mes+' '+dia+', '+anio+' 12:00:00');
	var meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio","Agosto","Septiembre", "Octubre","Noviembre", "Diciembre"]
	return `${dias[dt.getUTCDay()]} ${dia} de ${meses[dt.getUTCMonth()]} del ${anio}`;    
};

export default function ItemDetalle(props) {
	const classes = useStyles();

	console.log("INFO:",props.info);
	const {dt, humidity, speed, rain, sunrise, sunset} = props.info;
	const handleExpandClick = () => {
    props.setOpenDetails(dt);
	};

	//const formatted = moment(dt*1000).tz("America/Santiago").format("DD/MM/YYYY");
	const fechaChile = diaSemana(moment(dt*1000).tz("America/Santiago").format("DD"), moment(dt*1000).tz("America/Santiago").format("MM"), moment(dt*1000).tz("America/Santiago").format("YYYY"));
	const amanece = moment(sunrise*1000).tz("America/Santiago").format("HH:MM");
	const anochece = moment(sunset*1000).tz("America/Santiago").format("HH:MM");

	return (
		<Grid item xs={12} >
			<Paper>
				<CardActionArea onClick={handleExpandClick}> 
					<CardHeader
						avatar={
							<Avatar aria-label="recipe" className={parseInt(props.info.temp.day -273)>25?classes.avatarH:parseInt(props.info.temp.day -273)>15?classes.avatarM:classes.avatarL}>
								{parseInt(props.info.temp.day -273)}°
							</Avatar>
						}
						action={
							<IconButton
								className={clsx(classes.expand, {
									[classes.expandOpen]: props.expanded === dt?true:false,
								})}
								aria-expanded={props.expanded === dt?true:false}
								aria-label="show more"
								disableRipple
								disableFocusRipple
							>
								<ExpandMoreIcon />
							</IconButton>
						}
						title={<Typography>{fechaChile}</Typography>}
						subheader={`Max: ${parseInt(props.info.temp.max-273)}° - Min: ${parseInt(props.info.temp.min-273)}° `}
					/>
				</CardActionArea>
				<Collapse in={props.expanded === dt?true:false} timeout="auto" unmountOnExit style={{backgroundColor:"#80808047"}}>
					<CardContent>
						<Typography>Hora Amanecer: {amanece} - Hora Anochecer: {anochece} </Typography>
						<Typography>Humedad: {humidity} %</Typography>
						<Typography>Velocidad viento: {speed} km/h	</Typography>
						<Typography>Lluvia: {rain} %</Typography>
					</CardContent>
				</Collapse>
			</Paper>
		</Grid>
	);
}

