import axios from 'axios';

const axiosInstance = axios.create({
    baseURL: 'https://wqy3wijf3k.execute-api.us-east-1.amazonaws.com/dev'
});

export default axiosInstance;