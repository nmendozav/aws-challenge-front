import React from 'react';
import ReactDOM from 'react-dom';
import './assets/index.css';
import Home from './components/Home';

ReactDOM.render(<Home />, document.getElementById('root'));